package com.enterprise.system;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

@SpringBootTest
public class SmokeTest {

  @Autowired
  ApplicationContext app;

  @Test
  public void testSmoke() {
    assertNotNull(app);
  }
}
