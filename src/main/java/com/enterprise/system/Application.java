package com.enterprise.system;

import com.enterprise.system.dto.FinanDTO;
import com.enterprise.system.dto.FinanTokenDTO;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Slf4j
@SpringBootApplication
public class Application {

  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  @Autowired
  CommandLineRunner stratUp(OxmMarshallerWrapper oxm, JacksonWrapper jackson) {
    return (String... args) -> {
      final var installment = "946.2";
      final var date = "2021-12-20T00:00:00-03:00";
      final var lineSeparator = System.lineSeparator();
      final var finan01 = new FinanTokenDTO("526", "5200", "10", "1", installment, "AX", date, date, "",
          new FinanDTO("AIR", "VHI", installment, "CIA"));
      final var list = List.of(finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01, finan01,
          finan01, finan01, finan01);

      LOGGER.info("{}Marshalled object (oxm):{}{}", lineSeparator, lineSeparator, oxm.marshallXml(finan01));
      LOGGER.info("{}Marshalled object (jackson):{}{}", lineSeparator, lineSeparator, jackson.marshallXml(finan01));
      LOGGER.info("{}Marshalled objects (oxm):{}{}", lineSeparator, lineSeparator,
          list.stream().map(oxm::marshallXml).collect(Collectors.joining(lineSeparator)));
      LOGGER.info("{}Marshalled objects (jackson):{}{}", lineSeparator, lineSeparator,
          list.stream().map(jackson::marshallXml).collect(Collectors.joining(lineSeparator)));
      LOGGER.info("{}Marshalled objects (oxm, parallel):{}{}", lineSeparator, lineSeparator,
          list.parallelStream().map(oxm::marshallXml).collect(Collectors.joining(lineSeparator)));
      LOGGER.info("{}Marshalled objects (jackson, parallel):{}{}", lineSeparator, lineSeparator,
          list.parallelStream().map(jackson::marshallXml).collect(Collectors.joining(lineSeparator)));
    };
  }

  @Bean
  public Jaxb2Marshaller jaxb2Marshaller() {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setClassesToBeBound(FinanDTO.class, FinanTokenDTO.class);
    marshaller.setMarshallerProperties(Map.of(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true));
    // set more options...
    return marshaller;
  }

  @Bean
  public XmlMapper jacksonXmlMapper() {
    return XmlMapper.builder()
        .defaultUseWrapper(false)
        // enable/disable Features, change AnnotationIntrospector
        .build();
  }
}
