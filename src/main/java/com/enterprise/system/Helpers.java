package com.enterprise.system;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.StringWriter;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

@Component
class OxmMarshallerWrapper {

  final Jaxb2Marshaller marshaller;

  OxmMarshallerWrapper(Jaxb2Marshaller mler) {
    marshaller = mler;
  }

  public <T> String marshallXml(final T obj) {
    StringWriter sw = new StringWriter();
    Result result = new StreamResult(sw);
    marshaller.marshal(obj, result);
    return sw.toString();
  }
}

@Component
@Slf4j
class JacksonWrapper {

  final XmlMapper mapper;

  JacksonWrapper(XmlMapper mler) {
    mapper = mler;
  }

  public <T> String marshallXml(final T obj) {
    try {
      return mapper.writeValueAsString(obj);
    } catch (JsonProcessingException ex) {
      LOGGER.error(ex.getMessage(), ex);
      return ex.getMessage();
    }
  }
}
